# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2018-12-17

ad_page_contract {
  	Upload timesheet from timetracker for creation of tasks and logging hours

    @author malte.sussdorff@cognovis.de
    @creation-date 2018-12-17
} {
	{return_url ""}

}

set user_id [auth::require_login]

set page_title "Upload JIRA Issue CSV"

set project_options [im_project_options -exclude_subprojects_p 0 -exclude_status_id [im_project_status_closed] -exclude_type_id [list [im_project_type_ticket] [im_project_type_sla] [im_project_type_opportunity] [im_project_type_task]]]
set form_elements {
	{project_id:text(select)
		{label "[_ intranet-core.Project]"}
		{options $project_options}
	}
    {upload_file:file(file)
        {label "[_ acs-mail-lite.Upload_file]"}
    }
}

set edit_buttons [list [list [_ acs-mail-lite.Upload_file] Upload]]
ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name issue_upload \
    -export [list project_id freelance_package_id return_url return_file_p] \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -form $form_elements \
    -on_submit {
	    # Check that the uploaded filename matches
	    set upload_filepath [lindex $upload_file 1]
	    set upload_filename [lindex $upload_file 0]
	
	    set extension [file extension $upload_filename]
        set tmp_filename [ns_queryget upload_file.tmpfile]
        set encoding "utf-8"

		# Get the file contents
		if {[catch {
    		set fl [open $tmp_filename]
		    fconfigure $fl -encoding $encoding
		    set issue_content [read $fl]
		    close $fl
		} err]} {
		    ad_return_complaint 1 "Unable to open file $upload_file:<br><pre>\n$err</pre>"
		    return
		}

		set separator ","
		set issue_files [split $issue_content "\n"]
	    set issue_header [lindex $issue_files 0]
		set issue_headers [im_csv_split $issue_header $separator]
		set issue_entries [im_csv_get_values $issue_content $separator]


		# Defaults
		set cost_center_id [db_string default_cost_center "select department_id from im_employees where employee_id = :user_id" -default ""]
        set uom_id [im_uom_hour]

		set note ""

		foreach issue_entry $issue_entries {

			array unset issue_array 
			# Set the variables. Make sure to support multiple columns for e.g. comment
			for { set i 0 } { $i < [llength $issue_entry] } { incr i } {
		    	set key [lindex $issue_headers $i]
		    	set value [lindex $issue_entry $i]
			    if {[info exists issue_array($key)]} {
			       	catch {lappend issue_array($key) "$value"}
			    } else {
				    set issue_array($key) "$value"
		    	}
		  	}

		  	set username $issue_array(Assignee)
		  	set issue_user_id [db_string user_id "select user_id from users where username = :username" -default ""]

			# Custom for cognovis
			switch $issue_user_id {
				50656 {
					set material_id [db_string material_id "select material_id from im_materials where material_nr = 'co_extjs_hour'"]
				}
				default {
					set material_id [db_string material_id "select material_id from im_materials where material_nr = 'mt_support_hour'"]
				}
			}

			set task_name $issue_array(Summary)
			set task_nr $issue_array(Issue key)
			set creation_date $issue_array(Created)
			set resolution_date $issue_array(Resolved)
			set sprints $issue_array(Sprint)
			set jira_issue_id $issue_array(Issue id)
			set parent_issue_id $issue_array(Parent id)

		    set note $issue_array(Description)

			switch $issue_array(Status) {
				"Done" {
					set task_status_id 81
				}
				default {
					set task_status_id 76
				}
			}

			switch $issue_array(Issue Type) {
				"Story" {
					set task_type_id 9510
				}
				"Bug" {
					set task_type_id 9511
				}
				"Task" {
					set task_type_id 9512
				}
				"Sub-task" {
					set task_type_id 9513
				}

				default {
					set task_type_id 9500
				}
			}

		    # Check if the tasks exists
			set task_id [db_string task_id "select project_id from im_projects where project_nr = :task_nr" -default ""]
			if {$task_id eq ""} {
				# Try with name
				set task_id [db_string task_id "select project_id from im_projects where project_name = :task_name" -default ""]
			}
			if {$task_id eq ""} {
				set task_id [im_new_object_id]
				# Save the task
				db_string save_task "SELECT im_timesheet_task__new (
                	:task_id,               -- p_task_id
	                'im_timesheet_task',    -- object_type
    	            to_timestamp(:creation_date, 'DD/Mon/YY HH24:MI'),                  -- creation_date
        	        null,                   -- creation_user
            	    null,                   -- creation_ip
                	null,                   -- context_id

                	:task_nr,
                	:task_name,
                	:project_id,
	                :material_id,
    	            :cost_center_id,
        	        :uom_id,
            	    :task_type_id,
                	:task_status_id,
	                :note
	        	);"

			}

			db_dml update_task "update im_projects set project_status_id = :task_status_id, start_date = to_timestamp(:creation_date, 'DD/Mon/YY HH24:MI'), end_date = to_timestamp(:resolution_date, 'DD/Mon/YY HH24:MI') where project_id = :task_id"
			db_dml update_task "update im_timesheet_tasks set jira_issue_id = :jira_issue_id, task_status_id = :task_status_id, task_type_id = :task_type_id, material_id = :material_id, jira_sprints = :sprints where task_id=:task_id"

			if {$parent_issue_id ne ""} {
				set parent_task_id [db_string parent "select task_id from im_timesheet_tasks where jira_issue_id = :parent_issue_id" -default ""]
				if {$parent_task_id ne ""} {
					db_dml project_parent "update im_projects set parent_id = :parent_task_id where project_id = :task_id"
				}
			}
		}
	} -after_submit {
	    if {$return_url eq ""} {
		set return_url [export_vars -base "/intranet/projects/view" -url {project_id}]
	    }
	    
	    ad_returnredirect $return_url
    }

