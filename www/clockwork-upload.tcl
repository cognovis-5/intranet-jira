# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2018-12-17

ad_page_contract {
  	Upload timesheet from timetracker for creation of tasks and logging hours

    @author malte.sussdorff@cognovis.de
    @creation-date 2018-12-17
} {
	{return_url ""}

}

set user_id [auth::require_login]

set page_title "Upload Clockwork CSV"

set project_options [im_project_options -exclude_subprojects_p 0 -exclude_status_id [im_project_status_closed] -exclude_type_id [list [im_project_type_ticket] [im_project_type_sla] [im_project_type_opportunity] [im_project_type_task]]]
set form_elements {
	{project_id:text(select)
		{label "[_ intranet-core.Project]"}
		{options $project_options}
	}
    {upload_file:file(file)
        {label "[_ acs-mail-lite.Upload_file]"}
    }
}

set edit_buttons [list [list [_ acs-mail-lite.Upload_file] Upload]]
ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name clockwork_upload \
    -export [list project_id freelance_package_id return_url return_file_p] \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -form $form_elements \
    -on_submit {
	    # Check that the uploaded filename matches
	    set upload_filepath [lindex $upload_file 1]
	    set upload_filename [lindex $upload_file 0]
	
	    set extension [file extension $upload_filename]
        set tmp_filename [ns_queryget upload_file.tmpfile]
        set encoding "utf-8"

		# Get the file contents
		if {[catch {
    		set fl [open $tmp_filename]
		    fconfigure $fl -encoding $encoding
		    set clockwork_content [read $fl]
		    close $fl
		} err]} {
		    ad_return_complaint 1 "Unable to open file $upload_file:<br><pre>\n$err</pre>"
		    return
		}

		set separator ";"
		set clockwork_files [split $clockwork_content "\n"]
	    set clockwork_header [lindex $clockwork_files 0]
		set clockwork_headers [im_csv_split $clockwork_header $separator]
		set clockwork_entries [im_csv_get_values $clockwork_content $separator]
ds_comment "$clockwork_header"
		# Defaults
		set cost_center_id [db_string default_cost_center "select department_id from im_employees where employee_id = :user_id" -default ""]
        set uom_id [im_uom_hour]
        set company_id [db_string company_id "select company_id from im_projects where project_id = :project_id"]
		set note ""

		foreach clockwork_entry $clockwork_entries {
			template::util::list_to_array $clockwork_entry clockwork_array $clockwork_headers
			set task_name $clockwork_array(Issue Summary)
			set task_nr $clockwork_array(Issue Key)
			set creation_date $clockwork_array(Started at)
			set start_date $clockwork_array(Started at)
		    set hours_date $clockwork_array(Updated at)
		    set time_spent  $clockwork_array(Time spent)
		    set note $clockwork_array(Comment)



			set first_names [lrange [split $clockwork_array(Author)] 0 end-1]
			set last_name [lindex [split $clockwork_array(Author)] end]
			set hour_user_id [db_string user_id "select person_id from persons where first_names = :first_names and last_name = :last_name limit 1" -default ""]


		    # Check if the tasks exists
			set task_id [db_string task_id "select project_id from im_projects where project_nr = :task_nr" -default ""]

		    if {$task_id eq ""} {			
				# Custom for cognovis
				switch $hour_user_id {
					50656 {
						set material_id [db_string material_id "select material_id from im_materials where material_nr = 'co_extjs_hour'"]
					}
					default {
						set material_id [db_string material_id "select material_id from im_materials where material_nr = 'mt_support_hour'"]
					}
				}

				switch $clockwork_array(Issue Type) {
					"Story" {
						set task_type_id 9510
					}
					"Bug" {
						set task_type_id 9511
					}
					"Task" {
						set task_type_id 9512
					}
					"Sub-task" {
						set task_type_id 9513
					}

					default {
						set task_type_id 9500
					}
				}

				set task_status_id 81
	
				set task_id [im_new_object_id]
				# Save the task
				db_string save_task "SELECT im_timesheet_task__new (
                	:task_id,               -- p_task_id
	                'im_timesheet_task',    -- object_type
    	            :creation_date,                  -- creation_date
        	        null,                   -- creation_user
            	    null,                   -- creation_ip
                	null,                   -- context_id

                	:task_nr,
                	:task_name,
                	:project_id,
	                :material_id,
    	            :cost_center_id,
        	        :uom_id,
            	    :task_type_id,
                	:task_status_id,
	                ''
	        	);"
				db_dml update_task "update im_projects set project_status_id = :task_status_id, start_date = :creation_date, end_date = :hours_date where project_id = :task_id"
				db_dml update_task "update im_timesheet_tasks set task_status_id = :task_status_id, task_type_id = :task_type_id, material_id = :material_id where task_id=:task_id"
			}

			# Now add the hour logging 
			set time_spent_list [split $time_spent ":"]
			set hours_spent [string trimleft [lindex $time_spent_list 0] 0]
			if {$hours_spent eq ""} {set hours_spent 0}
			set minutes_spent [string trimleft [lindex $time_spent_list 1] 0]
			if {$minutes_spent eq ""} {
				set minutes_spent 0
			}

			set hours_worked [expr $hours_spent/0.25 *0.25]
			set hours_by_minutes [expr $minutes_spent/0.25*0.25 / 60]
			set hours_worked [expr $hours_worked + $hours_by_minutes]
		    set hour_id [db_string hours_exist "select hour_id from im_hours where user_id = :hour_user_id and project_id = :task_id and day = :hours_date" -default ""]
		

			if {$hour_id ne ""} {
				db_dml update "update im_hours set note = :note, hours = round(:hours_worked/0.25)*0.25 where user_id = :hour_user_id and project_id = :task_id and day = :hours_date"
				ns_log Notice "Updated Hours $hour_id for $task_nr - $first_names - $hours_worked - $minutes_spent - $hours_by_minutes - $note"

			} else {
				db_dml hours_insert "
	                insert into im_hours (
	                    	user_id, project_id,
	                        day, hours,
	                        note
	                     ) values (
	                        :hour_user_id, :task_id,
	                        :hours_date, round(:hours_worked/0.25)*0.25,
	                        :note
	                     )"
				ns_log Notice "Created Hours for $task_nr - $first_names - $hours_worked - $note"
			}
		}
	} -after_submit {
	    if {$return_url eq ""} {
			set return_url [export_vars -base "/intranet/projects/view" -url {project_id}]
	    }
	    
	    ad_returnredirect $return_url
    }

