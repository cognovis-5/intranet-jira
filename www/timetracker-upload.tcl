# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

# Template for email inclusion
# @author Malte Sussdorff (sussdorff@sussdorff.de)
# @creation-date 2018-12-17

ad_page_contract {
  	Upload timesheet from timetracker for creation of tasks and logging hours

    @author malte.sussdorff@cognovis.de
    @creation-date 2018-12-17
} {
	{return_url ""}

}

set user_id [auth::require_login]

set page_title "Upload Timetracker CSV"

set project_options [im_project_options -exclude_subprojects_p 0 -exclude_status_id [im_project_status_closed] -exclude_type_id [list [im_project_type_ticket] [im_project_type_sla] [im_project_type_opportunity] [im_project_type_task]]]
set form_elements {
	{project_id:text(select)
		{label "[_ intranet-core.Project]"}
		{options $project_options}
	}
    {upload_file:file(file)
        {label "[_ acs-mail-lite.Upload_file]"}
    }
}

set edit_buttons [list [list [_ acs-mail-lite.Upload_file] Upload]]
ad_form -action [ad_conn url] \
    -html {enctype multipart/form-data} \
    -name timetracker_upload \
    -export [list project_id freelance_package_id return_url return_file_p] \
    -cancel_label "[_ acs-kernel.common_Cancel]" \
    -cancel_url $return_url \
    -edit_buttons $edit_buttons \
    -form $form_elements \
    -on_submit {
	    # Check that the uploaded filename matches
	    set upload_filepath [lindex $upload_file 1]
	    set upload_filename [lindex $upload_file 0]
	
	    set extension [file extension $upload_filename]
        set tmp_filename [ns_queryget upload_file.tmpfile]
        set encoding "utf-8"

		# Get the file contents
		if {[catch {
    		set fl [open $tmp_filename]
		    fconfigure $fl -encoding $encoding
		    set timetracker_content [read $fl]
		    close $fl
		} err]} {
		    ad_return_complaint 1 "Unable to open file $upload_file:<br><pre>\n$err</pre>"
		    return
		}

		set separator ","
		set timetracker_files [split $timetracker_content "\n"]
	    set timetracker_header [lindex $timetracker_files 0]
		set timetracker_headers [im_csv_split $timetracker_header $separator]
		set timetracker_entries [im_csv_get_values $timetracker_content $separator]

		# Defaults
		set cost_center_id [db_string default_cost_center "select department_id from im_employees where employee_id = :user_id" -default ""]
        set uom_id [im_uom_hour]
        set company_id [db_string company_id "select company_id from im_projects where project_id = :project_id"]
		set note ""

		foreach timetracker_entry $timetracker_entries {
			template::util::list_to_array $timetracker_entry timetracker_array $timetracker_headers
			set task_name $timetracker_array(Issue Summary)
			set task_nr $timetracker_array(Issue Key)
			set creation_date $timetracker_array(Created)
			set start_date $timetracker_array(Start Time)
		    set hours_date $timetracker_array(Worklog Created)
		    set seconds_spent  $timetracker_array(Time Spent \(s\))
		    set note $timetracker_array(Worklog Description)


			set first_names [lrange [split $timetracker_array(User)] 0 end-1]
			set last_name [lindex [split $timetracker_array(User)] end]
			set hour_user_id [db_string user_id "select person_id from persons where first_names = :first_names and last_name = :last_name limit 1" -default ""]


		    # Check if the tasks exists
			set task_id [db_string task_id "select project_id from im_projects where project_nr = :task_nr" -default ""]
			if {$task_id eq ""} {
				# do Nothing
			} else {

				# Get the dates of the tasks
				db_1row project_dates "select to_char(start_date,'YYYY-MM-DD HH24:MI') as start_date, to_char(end_date,'YYYY-MM-DD HH24:MI') as end_date,
					to_char(to_timestamp(:hours_date, 'DD/Mon/YY HH24:MI'),'YYYY-MM-DD HH24:MI') as hours_date_ansi
					from im_projects where project_id = :task_id"

				set update_project_sql "project_id = :task_id"
				if {$hours_date_ansi < $start_date} {
					append update_project_sql ",start_date = :hours_date_ansi"
				}

				if {$hours_date_ansi > $end_date} {
					append update_project_sql ",end_date = :hours_date_ansi"
				}

				db_dml update_project "update im_projects set $update_project_sql where project_id = :task_id"

				# Now add the hour logging 
				set hours_worked [expr $seconds_spent / 3600.00]
			    set hour_id [db_string hours_exist "select hour_id from im_hours where user_id = :hour_user_id and project_id = :task_id and day = to_timestamp(:hours_date, 'DD/Mon/YY HH24:MI')" -default ""]
		
				if {$hour_id ne ""} {
					db_dml update "update im_hours set note = :note, hours = round(:hours_worked/0.25)*0.25 where user_id = :hour_user_id and project_id = :task_id and day = to_timestamp(:hours_date, 'DD/Mon/YY HH24:MI')"

					ns_log Notice "Updated Hours $hour_id for $task_nr - $first_names - $hours_worked - $seconds_spent - $note"

				} else {
					db_dml hours_insert "
	                    insert into im_hours (
	                    	user_id, project_id,
	                        day, hours,
	                        note
	                     ) values (
	                        :hour_user_id, :task_id,
	                        to_timestamp(:hours_date, 'DD/Mon/YY HH24:MI'), round(:hours_worked/0.25)*0.25,
	                        :note
	                     )"
					ns_log Notice "Created Hours for $task_nr - $first_names - $hours_worked - $seconds_spent - $note"
				}
			}
		}
	} -after_submit {
	    if {$return_url eq ""} {
			set return_url [export_vars -base "/intranet/projects/view" -url {project_id}]
	    }
	    
	    ad_returnredirect $return_url
    }

