-- 4.1.0.1.1-4.1.0.1.2.sql
SELECT acs_log__debug('/packages/intranet-jira/sql/postgresql/upgrade/upgrade-5.0.0.0.0-5.0.0.0.1.sql','');

SELECT im_category_new(9510, 'User Story', 'Intranet Gantt Task Type');
SELECT im_category_new(9511, 'Bug', 'Intranet Gantt Task Type');
SELECT im_category_new(9512, 'Task', 'Intranet Gantt Task Type');
SELECT im_category_new(9513, 'Sub-Task', 'Intranet Gantt Task Type');

update im_timesheet_tasks set task_type_id = 9510 where task_type_id = 10000023;

CREATE OR REPLACE FUNCTION inline_5 ()
RETURNS integer AS '
DECLARE
        v_attribute_id integer;
        v_count        integer;

BEGIN

      SELECT ida.attribute_id INTO v_attribute_id FROM im_dynfield_attributes ida, acs_attributes aa
      WHERE ida.acs_attribute_id = aa.attribute_id AND aa.object_type = ''im_timesheet_task'' AND aa.attribute_name = ''jira_sprints'';

      IF v_attribute_id = 0 THEN
         UPDATE acs_attributes SET pretty_name = ''Jira Sprint(s)'', pretty_plural = ''Jira Sprints'', min_n_values = 1, sort_order = 5
         WHERE attribute_id = (SELECT acs_attribute_id FROM im_dynfield_attributes WHERE attribute_id = v_attribute_id);
      ELSE
        v_attribute_id := im_dynfield_attribute_new (
        ''im_timesheet_task'',
        ''jira_sprints'',
        ''Jira Sprint(s)'',
        ''richtext'',
        ''text'',
        ''f'',
        8,
        ''f'',
        ''im_timesheet_tasks''
        );
	      -- Add column on table im_timesheet_tasks
    	  ALTER TABLE im_timesheet_tasks ADD COLUMN jira_sprints text;
      END IF;

	  UPDATE im_dynfield_type_attribute_map SET display_mode = ''edit'', required_p = ''t'' WHERE attribute_id = v_attribute_id;

      RETURN 0;
END;' language 'plpgsql';


SELECT inline_5 ();
DROP FUNCTION inline_5 ();


create or replace function inline_0 ()
returns integer as $body$
declare
	v_count			integer;
BEGIN
	select	count(*) into v_count
	from	user_tab_columns
	where	lower(table_name) = 'im_timesheet_tasks' and
		lower(column_name) = 'jira_issue_id';
	IF (v_count > 0) THEN return 1; END IF;

	alter table im_timesheet_tasks add jira_issue_id integer;

	return 0;
end;$body$ language 'plpgsql';
select inline_0();
drop function inline_0();